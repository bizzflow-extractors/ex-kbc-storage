# KBC storage extractor

## Description

Extractor for Keboola storage.

##### What it is used for?

Extracting the data from KBC storage to the specific folder on the local machine.

##### What the code does?

The code - especially main.py file does:

1. Load the config file, where are all necessary information for the extractor.
2. Connect to KBC by official python client and download specified tables.
3. Store tables to a specific folder on the local machine as CSV

## Requirements

* python3.9 or higher
* KBC token with access to storage

## How to use it?

### Configuration file
See example `config.sample.json` you can find out more in json schema `config.schema.json`


### Local use

1) Create or modify the config.json file (see example `config.sample.json`):

2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```

#### script parameters
 * --config \<path to config file\>
 * --output \<path to output directory>
 * --config-schema ../config.schema.json (path to json schema file)
