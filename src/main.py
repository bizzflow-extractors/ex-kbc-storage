import os

from bizztreat_base.config import Config

from extractor import KBCStorageExtractor


def main():
    conf = Config()
    extractor = KBCStorageExtractor(
        host=conf["host"],
        token=conf["token"],
    )
    for table_config in conf["tables"]:
        file_path = os.path.join(conf.output_folder, f"{table_config['output_table_name']}.csv")
        extractor.extract_table(
            table_id=table_config["kbc_table_id"],
            destination_file=file_path,
            changed_since=table_config.get("changed_since"),
            changed_until=table_config.get("changed_until"),
            columns=table_config.get("columns"),
        )


if __name__ == "__main__":
    main()
