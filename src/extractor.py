import logging
import shutil
import tempfile

from kbcstorage.client import Client

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class KBCStorageExtractor:
    def __init__(self, host: str, token: str, ):
        self.host = host
        self.token = token
        self.client = Client(host, token)

    def extract_table(self, table_id, destination_file,
                      changed_since=None,
                      changed_until=None,
                      columns=None,
                      ):
        logger.info(f"Extracting {table_id} into {destination_file}")
        with tempfile.TemporaryDirectory() as tmpdirname:
            extracted_file = self.client.tables.export_to_file(table_id=table_id,
                                                               path_name=tmpdirname,
                                                               changed_since=changed_since,
                                                               changed_until=changed_until,
                                                               columns=columns,
                                                               )
            shutil.move(extracted_file, destination_file)
        logger.info(f"Table {table_id} successfully was extracted into {destination_file}")
