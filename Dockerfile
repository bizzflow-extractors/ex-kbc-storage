FROM python:3.9-slim

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="KBC storage"
LABEL com.bizztreat.component="ex-kbc-storage"
LABEL com.bizztreat.title="KBC storage Extractor"

RUN apt-get update && apt-get install -y  git

ADD requirements.txt .
RUN pip install -r requirements.txt

ADD src/ /code
ADD config.schema.json /code

WORKDIR /code

CMD ["python3", "-u", "main.py"]
